#!/usr/bin/python3

import argparse
import dataset
import urllib.request
import json
import os


def get_api_key():
    """
    Get the YouTube API key
    """
    with open(".yt_api_key.txt", "r") as fh:
        key = fh.read().replace("\n", "")
    return key


def get_all_video_in_channel(api_key, channel_id):
    base_video_url = "https://www.youtube.com/watch?v="
    base_search_url = "https://www.googleapis.com/youtube/v3/search?"

    first_url = base_search_url + "key={}&channelId={}&part=snippet,id&order=date&maxResults=25".format(api_key, channel_id)

    results = []
    url = first_url
    while True:
        inp = urllib.request.urlopen(url)
        resp = json.load(inp)

        for i in resp["items"]:
            if i["id"]["kind"] == "youtube#video":

                video_id = i["id"]["videoId"]
                video_url = base_video_url + video_id
                channel = i["snippet"]["channelTitle"]
                title = i["snippet"]["title"]
                thumbnail_url = "https://img.youtube.com/vi/{}/mqdefault.jpg".format(video_id)

                results.append((video_id, video_url, channel, title, thumbnail_url))

        # Get more videos
        try:
            next_page_token = resp["nextPageToken"]
            url = first_url + "&pageToken={}".format(next_page_token)
        except:
            break

    return results


def download_image(img_url, out_fname, img_folder):
    assert out_fname.endswith(".jpg")
    urllib.request.urlretrieve(img_url, os.path.join(img_folder, out_fname))


def add_to_db(table, video_url, title, channel_name, thumb_url, thumb_local_name, clickbait):
    # Seems to be a bottleneck!
    table.insert(dict(
        video_url=video_url,
        title=title,
        channel_name=channel_name,
        thumb_url=thumb_url,
        thumb_local_name=thumb_local_name,
        clickbait=clickbait
    ))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Scrape youtube video data")
    parser.add_argument("--channel", help="Channel ID")
    parser.add_argument("--clickbait", type=int, choices=[0, 1], help="Mark all scraped videos as clickbait (1) or not (0).")
    parser.add_argument("--database", help="Path to the database")
    parser.add_argument("--table", help="table name", default="video_info")
    parser.add_argument("--images_folder", help="Folder where all scraped images will be saved")
    args = parser.parse_args()

    db = dataset.connect("sqlite:///" + args.database)
    table = db[args.table]
    api_key = get_api_key()

    vids_info = get_all_video_in_channel(api_key, args.channel)

    for i, (video_id, video_url, channel, title, thumbnail_url) in enumerate(vids_info):
        print("Downloading images: {}/{}".format(i, len(vids_info)), end="\r")
        local_img_name = video_id + ".jpg"

        download_image(thumbnail_url, local_img_name, args.images_folder)

        add_to_db(table, video_url, title, channel, thumbnail_url, local_img_name, args.clickbait)
