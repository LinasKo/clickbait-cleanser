#!/usr/bin/python3

from kivy.app import App
from kivy.core.window import Window
from kivy.uix.gridlayout import GridLayout
from kivy.uix.image import Image
from kivy.uix.checkbox import CheckBox
from kivy.base import EventLoop

from video_adapter import VideoAdapter
import os.path
from collections import OrderedDict


class MainScreen(GridLayout):
    """
    Basic Idea:
    ###############################
    #          #     😀	😁	😂	😃  #
    #          #     ▢  ▢  ▢  ▢   #
    #          #     ▢  ▢  ▢  ▢   #
    #          #     😄	😅  😆 😇   #
    ###############################
    #          #     😀	😁	😂	😃  #

    Icons represent different types of clickbait I'm aiming to capture.
    Icons for these types will need to be sourced from somewhere.

    """

    data_folder = "../data"

    def __init__(self, **kwargs):
        super(MainScreen, self).__init__(**kwargs)

        self.cols = 2
        self.image_widget = Image()
        self.add_widget(self.image_widget)
        self.checkbox_widget = CheckBoxScreen(**kwargs)
        self.add_widget(self.checkbox_widget)

        self.adapter = VideoAdapter()
        self.prev_video = None

        self.get_next()

    def key_action(self, *args):
        _, key_code, _, _, _ = args
        if key_code == 13:  # enter
            self.save_this()
            self.get_next()
        elif key_code == 32:  # space:
            self.save_this()
            self.get_next()

    def get_next(self):
        self.prev_video = self.adapter.get_video()
        if not self.prev_video:
            print("No unreviewed videos remaining in the database!")
            exit(0)
        EventLoop.window.title = self.prev_video["title"]
        self.image_widget.source = os.path.join(self.data_folder, self.prev_video["thumb_local_name"])
        self.checkbox_widget.set_checkboxes(self.prev_video)

    def save_this(self):
        if self.prev_video:
            checkbox_result = self.checkbox_widget.get_checkboxes()
            self.prev_video.update(checkbox_result)
            self.prev_video["processed"] = 1
            self.adapter.update_video(self.prev_video)
            self.prev_video = None


class CheckBoxScreen(GridLayout):
    def __init__(self, **kwargs):
        super(CheckBoxScreen, self).__init__(**kwargs)
        self.cols = 4
        self.padding = 30

        icons = [os.path.join("icons", icon_path) for icon_path in [
            "arrow.png", "circle.png", "outline.png", "colorful.png",
            "flashy_text.png", "face.png", "title.png", "suggestive.png"
        ]]
        db_columns = [
            "cb_arrow", "cb_circle", "cb_outline", "cb_vivid_colors",
            "cb_flashy_text", "cb_emotional_face", "cb_title", "cb_suggestive"
        ]
        assert len(icons) == 8 and len(db_columns) == len(icons)

        self.cb_widgets = OrderedDict([(db_column, CheckBox()) for db_column in db_columns])

        for i in range(4):
            self.add_widget(Image(source=icons[i]))
        for cb in self.cb_widgets.values():
            self.add_widget(cb)
        for i in range(4, 8):
            self.add_widget(Image(source=icons[i]))

    def set_checkboxes(self, video_dict):
        for db_column, cb in self.cb_widgets.items():
            cb.active = video_dict[db_column] == 1

    def get_checkboxes(self):
        result = {}
        for db_column, cb in self.cb_widgets.items():
            result[db_column] = float(cb.active)
        return result


class MyApp(App):
    def build(self):
        self.title = "Clickbait Cleanser - data annotator"
        main_widget = MainScreen()
        Window.bind(on_key_down=main_widget.key_action)
        return main_widget


if __name__ == '__main__':
    MyApp().run()
