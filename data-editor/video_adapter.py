import dataset


class VideoAdapter:

    def __init__(self):
        self.db = dataset.connect("sqlite:///../data/video_info.db")
        self.table = self.db["video_info"]

        fields_to_be_updated = [col for col in self.table.columns if col.startswith("cb_")]
        fields_to_be_updated.append("processed")
        assert len(fields_to_be_updated) == 9
        self.fields_to_filter_by = [col for col in self.table.columns if col not in fields_to_be_updated]

    def get_video(self):
        vid = self.table.find_one(processed=0)
        return vid

    def update_video(self, video):
        self.table.update(video, self.fields_to_filter_by)
