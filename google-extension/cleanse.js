let videoRootSelector = "#dismissable.ytd-grid-video-renderer";
let thumbNodeSelector = "a#thumbnail .ytd-thumbnail img#img";
let titleNodeSelector = "#details h3 a#video-title"


function selectAllVideos() {
	let videoNodes = document.querySelectorAll(videoRootSelector);
	return videoNodes;
}

function isCleansed(videoNode) {
	// Arial label on title nodes seems to match the title closely
	let titleNode = getTitleNode(videoNode);
	let prevAriaLabel = titleNode.getAttribute("pre-clickbait-aria-label");
	return prevAriaLabel && prevAriaLabel == titleNode.getAttribute("aria-label");
}

function setCleansed(videoNode) {
	// Arial label on title nodes seems to match the title closely
	let titleNode = getTitleNode(videoNode);
	let ariaLabel = titleNode.getAttribute("aria-label");
	titleNode.setAttribute("pre-clickbait-aria-label", ariaLabel);
}

function getThumbNode(videoNode) {
	let thumbNodes = videoNode.querySelectorAll(thumbNodeSelector);
	if (thumbNodes.length != 1)
	{
		console.log(`Expected one thumbnail per video but found ${thumbNodes.length}.`);
		return undefined;
	}
	// can be undefined if image did not load yet
	return thumbNodes[0];
}

function getThumb(thumbNode) {
	// can be undefined if image did not load yet
	return thumbNode.src;
}

function setThumb(thumbNode) {
	if (getThumb(thumbNode) != undefined) {
		thumbNode.src = chrome.runtime.getURL("images/placeholder_210x118.png");
	}
}

function getTitleNode(videoNode) {
	let titleNodes = videoNode.querySelectorAll(titleNodeSelector);
	if (titleNodes.length != 1)
	{
		console.log(`Expected one title per video but found ${titleNodes.length}.`);
		return undefined;
	}
	return titleNodes[0];
}

function getTitle(titleNode) {
	return titleNode.textContent;
}

function setTitle(titleNode, text) {
	titleNode.textContent = text;
}

function removeDisallowedChars(text) {
	let severityScore = Math.pow(2, text.split("!").length-1) - 1
	text = text.replace(/!/g, '');
	return [text, severityScore]
}

function stopScreaming(text) {
	let severityScore = 0;

	let words = text.split(" ");
	for (i = 0; i < words.length; i++) {
		let word = words[i];
		let atLeastOneUpper = false;
		let allCaps = true;
		if (word.length >= 3) {
			word.split('').forEach(function(char) {
				atLeastOneUpper = atLeastOneUpper || /[A-Z]/.test(char);
				allCaps = allCaps && !/[a-z]/.test(char);
			});
			if (atLeastOneUpper && allCaps) {
				severityScore += word.length;
				words[i] = '';
			};
		};
	};
	text = words.join(' ');
	return [text, severityScore];
}

function removeHashTags(text) {
	text = text.replace(/#[a-zA-Z][a-zA-Z][a-zA-Z]+/g, '');
	return [text, 0];
}

function removeNumbers(text) {
	text = text.replace(/[0-9]/g, '');
	return [text, 0];
}

function cleanUp(text) {
	// Remove weird prefixes, resulting from the cleanse
	text = text.replace(/ +/g, ' ');
	text = text.replace(/^[ \.\-\|]+/g, '');
	return text;
}

function cleanseTitle(titleString) {
	let severityScore = 0;
	let extraSeverityScore;

	[titleString, extraSeverityScore] = removeDisallowedChars(titleString);
	severityScore += extraSeverityScore;

	[titleString, extraSeverityScore] = stopScreaming(titleString);
	severityScore += extraSeverityScore;

	[titleString, extraSeverityScore] = removeHashTags(titleString);
	severityScore += extraSeverityScore;

	// Remove numerical
	if (severityScore >= 4) {
		[titleString, extraSeverityScore] = removeNumbers(titleString);
		severityScore += extraSeverityScore;
	};

	titleString = cleanUp(titleString);
	return titleString;
}


function cleanseClickbait() {
	videoNodes = selectAllVideos();

	videoNodes.forEach(function(videoNode) {
		if (!isCleansed(videoNode)) {
			let thumbNode = getThumbNode(videoNode);
			setThumb(thumbNode);

			let titleNode = getTitleNode(videoNode);
			let titleString = getTitle(titleNode);
			setTitle(titleNode, cleanseTitle(titleString));

			setCleansed(videoNode);
		}
	});
}

window.onload = function() { cleanseClickbait(); };
window.setInterval(function() { cleanseClickbait(); }, 250);
